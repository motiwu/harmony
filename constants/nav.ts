export const nav = [
  {
    label: 'HOME',
    link: '/',
    class: 'home'
  },
  {
    label: 'COMPANY',
    link: '/company',
  },
  {
    label: 'PRODUCTS',
    link: '/products',
  },
  {
    label: 'NEWS',
    link: '/news',
  },
  {
    label: 'CONTACTS',
    link: '/contacts',
  },
]
