export const products = [
  {
    label: 'Printing Machine',
    id: 'printing',
    link: '/products/printing',
    image: 'products/1.png'
  },
  {
    label: 'Laminator',
    id: 'laminator',
    link: '/products/laminator',
    image: 'products/2.png'
  },
  {
    label: 'Slitter / Rewinder',
    id: 'slitter_rewinder',
    link: '/products/slitter_rewinder',
    image: 'products/3.png'
  },
  {
    label: 'Pouch Making Machine',
    id: 'pouch',
    link: '/products/pouch',
    image: 'products/4.png'
  },
  {
    label: 'PVC / PET Shrink Label Making Machine',
    id:'pvc_pet',
    link: '/products/c_pet',
    image: 'products/5.png'
  },
  {
    label: 'Sheeter',
    id: 'sheeter',
    link: '/products/sheeter',
    image: 'products/6.png'
  },
  {
    label: 'Rewinder',
    id: 'rewinder',
    link: '/products/rewinder',
    image: 'products/7.png'
  },
  {
    label: 'Related Machinery',
    id: 'related',
    link: '/products/related',
    image: 'products/8.png'
  },
]

export const categories = {
  printing: {
    models: [
      {
        label: 'JHB-T',
        id: 'JHB-T',
        thumb: 'products/printing/a.png',
        image: 'products/printing/JHB-T.png'
      },
      {
        label: 'JHB-TT',
        id: 'JHB-TT',
        thumb: 'products/printing/a.png'
      },
      {
        label: 'JHB-TTT',
        id: 'JHB-TTT',
        thumb: 'products/printing/a.png'
      },
    ]
  }
}